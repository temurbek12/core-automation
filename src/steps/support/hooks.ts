import {BeforeAll, Before, After, AfterAll} from '@cucumber/cucumber';
import { chromium } from "@playwright/test";




BeforeAll(async() =>{
    global.browser = await chromium.launch({
        headless: false
    })
});


AfterAll(async() => {
    await global.browser.close();
})


Before(async(scenario) => {
    global.context = await global.browser.newContext({
        recordVideo : {
            dir : 'videos/'+scenario.pickle.name,
        }
    });
    global.context = await global.browser.newContext();
    global.page = await global.context.newPage();
})

After (async() =>{
    await global.page.close();

})

module.exports = {
    default: '--publish-quiet',
};